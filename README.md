# Build image locally

~~~
cd ./provisioning
docker build --tag bld_db -f docker/Dockerfile_debug .
~~~

# Run container with api locally

~~~
docker run -it -d --name bld_db -p 127.0.0.1:3306:3306 bld_db
~~~